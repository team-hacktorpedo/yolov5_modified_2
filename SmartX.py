
from math import sqrt
import cv2
import numpy as np
import math


#Perspective transformation of input
class SmartX_PerspectiveTransform:
    def __init__(self, dwith= (1020, 1020)):
        self.width = dwith[0]
        self.height = dwith[1]
        #Perspective Matrix
        input = np.float32([[406,420], [1504,400], [1960,1002], [-28,1020]])
        output = np.float32([[0,0], [self.width-1,0], [self.width-1,self.height-1], [0,self.height-1]])

        self.matrix = cv2.getPerspectiveTransform(input,output)



    def EvenOutParkingLot(self, img):
        #Return Warped image
        return cv2.warpPerspective(img, self.matrix, (self.width,self.height), cv2.INTER_LINEAR, borderMode=cv2.BORDER_CONSTANT, borderValue=(0,0,0))


def IsCarInsideParkinglot(parpkatzsize, parkplaeze, checkvalue, yperspectifMultiplikator=-0.1, Linearverschibung=160):
    intersecting_ValueX = 0
    intersecting_ValueY = 0
    intersections = 0
    actualbiggestintersection = 0
    zeler = 0

    #mittelpunktverschibung anhand gradienten
    checkvalue = [checkvalue[0], checkvalue[1] + (checkvalue[1] * yperspectifMultiplikator + Linearverschibung)]

    for row in parkplaeze:
        TopL = row + parpkatzsize[0]
        BotR = row + parpkatzsize[1]
        #midpoint inside box
        if checkvalue[0] > TopL[0] and checkvalue[0] < BotR[0] and checkvalue[1] > TopL[1] and checkvalue[1] < BotR[1]:
            print("inpox")
            #for futur use distance from corner
            if checkvalue[0] - TopL[0] > 0 and BotR[0] - checkvalue[0] > 0:
                intersecting_ValueX = BotR[0] - checkvalue[0]
            if checkvalue[1] - TopL[1] > 0 and BotR[1] - checkvalue[1] > 0:
                intersecting_ValueY = BotR[1] - checkvalue[1]

            intersection = math.sqrt(intersecting_ValueX*intersecting_ValueX + intersecting_ValueY*intersecting_ValueY)
            #Parking Lot selection
            if intersection > actualbiggestintersection:
                actualbiggestintersection = intersection
                intersections = zeler
                print(intersections)
        zeler+=1
    return intersections


